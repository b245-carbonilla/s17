/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/	
	
	//first function here:
	function personalInfo () {
	let fullName = prompt("What is your name? ");
	let currentAge = prompt("What is your age? ");
	let currentAddress = prompt("Where do you live? ");

	console.log("Hello, "+ fullName);
	console.log("You are " + currentAge + " years old");
	console.log("You live in " + currentAddress);

	}
	personalInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFavoriteBand() {
		console.log("1. Cueshe");
		console.log("2. Hale");
		console.log("3. Parokya ni Edgar");
		console.log("4. Eraserheads");
		console.log("5. December Avenue");
	}

	displayFavoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayFavoriteMovie() {
		console.log("The Lord of the Rings: The Fellowship of the Ring"); 
		console.log("Rotten Tomatoes Rating: 91%");
		console.log("The Lord of the Rings: The Two Towers"); 
		console.log("Rotten Tomatoes Rating: 95%");
		console.log("The Lord of the Rings: The Return of the King"); 
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("The Lord of the Rings: The Rings of Power"); 
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("The Lord of the Rings: The Desolation of Smaug"); 
		console.log("Rotten Tomatoes Rating: 74%");
	}

	displayFavoriteMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers(); //should be in the end
function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);  //friends to friend3
}

// console.log(friend1);
// console.log(friend2);
printUsers();
